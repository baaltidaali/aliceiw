import logo from './logo.svg';
import './App.css';
import { Button,Modal } from 'react-bootstrap';
import axios from 'axios';
import { useEffect,useState } from 'react';
import {FormControl,Form} from 'react-bootstrap';
import {  DataGrid, GridRowsProp, GridColDef }from '@mui/x-data-grid';
import { styled, Box } from '@mui/system';
import ModalUnstyled from '@mui/base/ModalUnstyled';

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.5);
`;

const Backdrop = styled('div')`
  z-index: -1;
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const style = {
  width: "40vw",
  height:"40vh",
  bgcolor: 'white',
  border: '2px solid #000',
  p: 2,
  px: 4,
  pb: 3,
  overflow:"scroll",
};


function App() {

  const [retour,setRetour]=useState("fgqgreqg")
  const [pageSize, setPageSize] = useState(3);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([])
  const [name,setName]=useState("")
  const [search, setSearch] = useState("")
  const [show, setShow] = useState(false);
  const[selection,setSelection]=useState(null)

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const fetchData = async () =>{
    setLoading(true);
    try {
      const {data: response} = await axios.get('https://entreprise.data.gouv.fr/api/sirene/v1/full_text/'+name+'?per_page=5&page=1');
      setData(response);
    } catch (error) {
      console.error(error.message);
      setData(null)
    }
    setLoading(false);
  }

  useEffect(()=>{
    fetchData();
  },[name])


  

  const columns = [
    { field: 'siren', headerName: 'siren', width: 150 },
     { field: 'l1_normalisee', headerName: 'Name', width: 460 },

  ];
  return (
    <div className="App" style={{marginTop:"10vh"}}>
      {console.log(data)}
      <Form className="d-flex">
        <FormControl
          type="search"
          placeholder="Search"
          className="me-2"
          aria-label="Search"
          onChange={(e)=>{setSearch(e.target.value)}}
        />
        <Button variant="outline-success" onClick={()=>{setName(search)}}>Search</Button>
      </Form>
      <div style={{ height: '40vh', width: '40vw',margin:"5vh 30vw 30vh 30vw" }}>
      <DataGrid 
      pageSize={pageSize}
      onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
      rowsPerPageOptions={[1, 2, 3,10]}
      pagination
      rows={data?data.etablissement:[]} columns={columns} onRowClick={(e)=>{setSelection(data.etablissement.filter((s)=>{return e.row.id===s.id }))
      setShow(true)}} />
    </div>

    <StyledModal
  aria-labelledby="unstyled-modal-title"
  aria-describedby="unstyled-modal-description"
  open={show}
  onClose={handleClose}
  //BackdropComponent={Backdrop}
>
  <Box sx={style}>
  {selection ?JSON.stringify(selection,null,2):null}
  </Box>
</StyledModal>


{/* <Button onClick={()=>{setShow(true)}}>here</Button>
    
    <div><pre>
    {selection ?JSON.stringify(selection,null,2):null}</pre>
    </div> */}

 
    </div>
  );
}

export default App;
